__author__ = 'Marcin'

class Osoba:
    def __init__(self, imie, nazwisko, oceny):
        self.imie = imie
        self.nazwisko = nazwisko
        self.oceny = oceny.rstrip('\n') #usnięcie EOF
        OcenList = [float(s) for s in self.oceny.split()] #lista
        self.FloatListGrade = list(map(float, OcenList))
        self.average()
    def average(self):
        sum =0
        count = 0
        for ocena in self.FloatListGrade:
            sum += ocena
            count += 1

        self.srednia =  sum/count
    def wypisz(self):
        print('--------------------------')
        print('Imie: ' + self.imie + ' ' + self.nazwisko)
        print('Oceny: ' + ' '.join(map(str, self.oceny)))
        print('Srednia: ' + str(self.srednia))
        print('--------------------------')

listaOsob = []

plik = open('oceny.txt', 'r')

for linia in plik:
    tmp = linia.split(" ")
    listaOcen = " ".join(tmp[2:])
    listaOsob.append(Osoba(tmp[0], tmp[1], listaOcen))

plik.close()

for obj in listaOsob:
    obj.wypisz()